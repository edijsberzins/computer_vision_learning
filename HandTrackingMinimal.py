import cv2
import mediapipe as mp
import time

cap = cv2.VideoCapture(-1)

mp_hands = mp.solutions.hands
hands = mp_hands.Hands()
mp_draw = mp.solutions.drawing_utils

past_time = 0
current_time = 0

while True:
    success, img = cap.read()
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    results = hands.process(img_rgb)

    if results.multi_hand_landmarks:
        for hand_landmarks in results.multi_hand_landmarks:
            for id, landmark in enumerate(hand_landmarks.landmark):
                h, w, c = img.shape
                cx, cy = int(landmark.x*w), int(landmark.y*h)
                if id == 8:
                    cv2.circle(img, (cx, cy), 10, (150,80,0), cv2.FILLED) # Show blue circle on tip of index finger

            mp_draw.draw_landmarks(img, hand_landmarks, mp_hands.HAND_CONNECTIONS)

    current_time = time.time()
    fps = 1/(current_time-past_time)
    past_time = current_time

    cv2.putText(img, f'{str(int(fps))} fps', (10,70), cv2.FONT_HERSHEY_PLAIN, 3, (150,80,0), 3)

    cv2.imshow("Image", img)
    cv2.waitKey(1)
