import cv2
import mediapipe as mp
import time
import HandTrackingModule as htm

past_time = 0
current_time = 0
cap = cv2.VideoCapture(-1)

detector = htm.HandDetector()

while True:
    success, img = cap.read()
    img = detector.find_hands(img, draw=True)
    lmList = detector.find_position(img, draw=True)
    # if len(lmList) != 0:
    #     print(lmList[8])

    current_time = time.time()
    fps = 1/(current_time-past_time)
    past_time = current_time

    cv2.putText(img, f'{str(int(fps))} fps', (10,70), cv2.FONT_HERSHEY_PLAIN, 3, (150,80,0), 3)

    cv2.imshow("Image", img)
    cv2.waitKey(1)
